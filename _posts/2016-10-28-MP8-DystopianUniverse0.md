---
title: "Episódio 8 - Mesa 0: preparação de campanha (Dystopian Universe Playtest)"
subheadline: Preparando o terreno para as Mesas Predestinadas de Dystopian Universe
date: 2016-10-29 09:20:00 -0200
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadas
 - Podcast
 - Dystopian Universe
header: no
maisquatro_comments: true 
podcast_time: 49min
itunes:
  duration: 49:28
hosts:
  - Fábio Emilio Costa (Narrador)
  - Luiz Cavalheiro (Jean-Jacques le Blanc)
  - Paulo "Meirense" França (Remy Fayat)
audios:
 - OGG: https://archive.org/download/MPEp8DystopianUniverse0/MP-Ep8-DystopianUniverse0.ogg
 - MP3: https://archive.org/download/MPEp8DystopianUniverse0/MP-Ep8-DystopianUniverse0.mp3
iaplayer: MPEp8DystopianUniverse0
comments: true
related_links:
  - text: _Interface Zero_
    link: http://www.retropunk.net/editora/interface-zero/
  - text: _TekWar_
    link: https://en.wikipedia.org/wiki/TekWar
  - text: _Ficha da Remy_
    link: /personagens/RemyFayat/
  - text: _Ficha do Jean-Jacques_
    link: /personagens/JeanJacquesLeBlanc/
  - text: _Ficha Inicial do Governo_
    link: /personagens/Governo/
  - text: _Ficha Inicial da __La Resistènce___
    link: /personagens/LaResistence/
events:
  - time: "00:00:08"
    text: Introdução às Mesas Predestinadas de _Dystopian Universe_ e explicação de Sessão 0
  - time: "00:01:24"
    text: Introdução ao Cenário de _Dystopian Universe_ e aos primeiros jogadores
  - time: "00:04:03"
    text: Criando _La Resistènce_, escolhendo _Manifesto_, _Fraqueza_ e os Avanços da mesma
  - time: "00:22:01"
    text: Criando o _Governo_, escolhendo _Slogan_, _Escândalo_ e Avanços
  - time: "00:34:01"
    text: Escolhendo os segredos iniciais dos Personagens do Luiz e do Meirense
youtube_table: T1abQe91vVE
soundtrack:
  - music: Lewd Day
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Gorgonzola
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Anmbient Gourd
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Trumpet Backdrone
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: "Mary M's Space Music"
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Where the Honey Is
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Bassomatic
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: 303x909 (Remix)
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Gourd Travel
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  # - music: Cost of Difference
  #   artist: Canton Becker
  #   link: http://cantonbecker.com/music/
  # - music: Floodwater
  #   artist: Canton Becker
  #   link: http://cantonbecker.com/music/
  # - music: Map of the Cosmos
  #   artist: Canton Becker
  #   link: http://cantonbecker.com/music/
  # - music: Schvisen
  #   artist: Canton Becker
  #   link: http://cantonbecker.com/music/
  # - music: Stringy
  #   artist: Canton Becker
  #   link: http://cantonbecker.com/music/
  # - music: Sunflower
  #   artist: Canton Becker
  #   link: http://cantonbecker.com/music/
  # - music: The Great River
  #   artist: Canton Becker
  #   link: http://cantonbecker.com/music/
---

Olá!

A Oitava Mesa Predestinada é um tanto diferente, onde apresentamos a primeira Mesa Predestinada (na verdade, a _Sessão 0_)

Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.


[fatemasters]: http://fatemasters.github.io
[rolandomaisquatro]: http://rolandomaisquatro.github.io
[camundongos-aventureiros]: https://pt.wikipedia.org/wiki/The_Country_Mouse_and_the_City_Mouse_Adventures
