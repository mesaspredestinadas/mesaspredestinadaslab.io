---
title: La Resistènce - Final
subheadline: A Resistência contra o Governo de Paris Nouveau para _Dystopian Universe_
layout: personagens
language: en
categories:
  - personagens
tags:
  - Dystopian Universe Beta Playtest Campaign
  - materiais
header: no
---

## Aspectos

| **Tipo** | **Aspecto** |
|-|-|
| *Manifesto* | Na _Treillis_ ou na _Apogeè_, traremos de volta as pessoas para a realidade |
| *Fraqueza*  | Poucos, Espalhados e com múltiplas Agendas |

## Avanços

+ _Hijacked Shipments_
+ _Hearts and Minds_
+ _Training Archive Access_
+ _Webs of Informants_
