---
title: O Governo - Final
subheadline: A opressora sociedade de _Dystopian Unverse_ que persegue a Resistência
layout: personagens
language: en
categories:
  - personagens
tags:
  - Dystopian Universe Beta Playtest Campaign
  - materiais
header: no
---

## Aspectos

| **Tipo** |  **Aspecto** |
|-|-|
| *Slogan*     | _"Por uma Paris Nouveau mais segura"_ |
| *Escândalo:* | _A AR/VR é um vício literal e lucrativo_ |

### Avanços (5)

+ Surveillance Optimization -  +2 Blowback / scene
+ Investing In Infrastructure - Can transfer 2 FP from GM Pool to Bank
+ Informers - +2 Blowback / scene
+ Wage Negotiation - 2 FP to the Bank at Prep Scene
+ Increased Security Presence - +1 General Opposition
+ Constant Vigilance - +2 General Opposition
+ Income Tax Increase - 1 FP to the Bank at Prep Scene


| *** Informações *** | ***Valor*** |
|-|-|
| Banco | 7 |
| Avanços | 0 |
| Blowback | 17 |
