---
title: "Episódio 9 - Dystopian Universe Mesa 1: Invasão de Armazém"
subheadline: Uma simples Invasão de Armazém se mostra muito mais complexa no fundo
date: 2016-11-02 17:00:00 -0200
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadas
 - Podcast
 - Dystopian Universe
header: no
maisquatro_comments: true 
podcast_time: 110min
itunes:
  duration: 01:50:19
hosts:
  - Fábio Emilio Costa (Narrador)
  - Luiz Cavalheiro (Jean-Jacques le Blanc)
  - Paulo "Meirense" França (Remy Fayat)
audios:
 - OGG: https://archive.org/download/MPEp9DystopianUniverse1/MP-Ep9-DystopianUniverse1.ogg
 - MP3: https://archive.org/download/MPEp9DystopianUniverse1/MP-Ep9-DystopianUniverse1.mp3
iaplayer: MPEp9DystopianUniverse1
comments: true
related_links:
  - text: _Ficha da Remy_
    link: /personagens/RemyFayat/
  - text: _Ficha do Jean-Jacques_
    link: /personagens/JeanJacquesLeBlanc/
  - text: _Cemitério Père-Lachaise_
    link: https://pt.wikipedia.org/wiki/Cemit%C3%A9rio_do_P%C3%A8re-Lachaise
events:
  - time: "00:00:08"
    text: Introdução à aventura e ao cenário da aventura
  - time: "00:03:18"
    text: Jean-Jacques e Remy se encontram no Cemitério _Père-Lachaise_
  - time: "00:22:37"
    text: Said e Leandra aparecem e passam para Jean-Jacques e Remy a missão, e a desconfiança rola solta
  - time: "00:39:15"
    text: Jean-Jacques e Remy fazem as suas preparações na _prep scene_
  - time: "00:42:07"
    text: Remy decide preparar alguma vantagem de fuga por meio de _Metadados Falsos_
  - time: "00:46:18"
    text: Jean-Jacques observa o armazém e com isso compreende os _Turnos e Métodos_ do mesmo que possam ser explorados na hora de entrar e/ou sair
  - time: "00:46:18"
    text: Jean-Jacques observa o armazém e com isso compreende os _Horários e Métodos_ que possam ser explorados na hora de entrar e/ou sair
  - time: "00:56:12"
    text: A Missão começa com Jean-Jacques e Remy recebendo de _La Resistènce_ oferecendo um furgão e alguns _Naturels_ como _Funcionários Fantasmas_
  - time: "00:59:19"
    text: Após entrar no Armazém, Remy se conecta a um ponto de acesso e se torna "invisível" aos leitores de metadados e obtem a localização dos guardas
  - time: "01:03:19"
    text: Remy gera um _Manifesto de Carga Falso_ para pegar a carga, enquanto os Naturals fazem a carga do furgão
  - time: "01:09:04"
    text: Jean-Jacques procura o _Chefe de Segurança_ Rodriguez para ele assinar o _Manifesto de Carga Falso_ e tenta ver mais sobre o _Project ET-01_
  - time: "01:18:40"
    text: Remy não consegue se segurar e deixa uma mensagem para trás... E são descoberto poucos instantes depois de sairem do armazém! Estão sendo caçados!
  - time: "01:20:19"
    text: Os membros de _La Resistence_ descaracterizam o furgão
  - time: "01:20:19"
    text: Os membros de _La Resistence_ conseguem arrastar a carga roubada pelos esgotos e jogam o furgão no Seine
  - time: "01:32:30"
    text: Começa o _debrief_, e Leandra não está nada satisfeita com a bagunça feita pelos nossos "heróis"
  - time: "01:33:22"
    text: É revelado o fato de Remy ter deixado uma mensagem para trás
  - time: "01:34:52"
    text: Remy é acusada de traição e revela o fato de que ela se levou pela paixão
  - time: "01:37:28"
    text: Encerramento da Aventura com os Avanços de _La Resistènce_ e do _Governo_
  - time: "01:39:30"
    text: Comentários finais sobre a aventura
youtube_table: "T1abQe91vVE#t=58m37s"
soundtrack:
  - music: Lewd Day
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Gorgonzola
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Anmbient Gourd
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Trumpet Backdrone
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: "Mary M's Space Music"
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Where the Honey Is
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Bassomatic
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: 303x909 (Remix)
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Gourd Travel
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Cost of Difference
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Floodwater
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Map of the Cosmos
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Schvisen
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Stringy
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Sunflower
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: The Great River
    artist: Canton Becker
    link: http://cantonbecker.com/music/
---

Olá!

Na Mesa Predestinada 9, começam as aventuras de Remy e Jean-Jacques, os combatentes da _La Resisténce_, enfrentando o opressor governo de _Paris Nouveau_ e procurando derrubar o mesmo.

Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.


[fatemasters]: http://fatemasters.github.io
[rolandomaisquatro]: http://rolandomaisquatro.github.io
[camundongos-aventureiros]: https://pt.wikipedia.org/wiki/The_Country_Mouse_and_the_City_Mouse_Adventures
