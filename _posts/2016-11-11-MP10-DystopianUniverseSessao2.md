---
title: "Episódio 10 - Dystopian Universe Mesa 2: Espionagem Corporativa"
subheadline: Os nossos combatentes da Liberdade invadem uma das grandes corporações atrés do _Project ET-01_, que pode ter pistas quanto a _Transformation De La Tèrre_
date: 2016-11-11 16:25:00 -0200
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadas
 - Podcast
 - Dystopian Universe
header: no
maisquatro_comments: true 
podcast_time: 175min
itunes:
  duration: 02:55:27
hosts:
  - Fábio Emilio Costa (Narrador)
  - Luiz Cavalheiro (Jean-Jacques le Blanc)
  - Paulo "Meirense" França (Remy Fayat)
  - César Hitos Araújo (Camille Lejardins)
  - Silas Marques de Lima (Martin Le Marteur)
audios:
 - OGG: https://archive.org/download/MPEp10DystopianUniverse2/MP-Ep10-DystopianUniverse2.ogg
 - MP3: https://archive.org/download/MPEp10DystopianUniverse2/MP-Ep10-DystopianUniverse2.mp3
iaplayer: MPEp10DystopianUniverse2
comments: true
related_links:
  - text: _Ficha da Remy_
    link: /personagens/RemyFayat/
  - text: _Ficha do Jean-Jacques_
    link: /personagens/JeanJacquesLeBlanc/
  - text: _Ficha do Martin Marteur_
    link: /personagens/MartinLeMarteur/
  - text: _Ficha do Camille Desjardins_
    link: /personagens/CamilleDesjardins/
  - text: _Ficha Final do Governo_
    link: /personagens/Governo-Final/
  - text: _Ficha Final da __La Resistènce___
    link: /personagens/LaResistence-Final/
events:
  - time: "00:00:11"
    text: Introdução e apresentação dos novos personagens
  - time: "00:02:24"
    text: O que Jean-Jacques e Remy fizeram no meio-tempo
  - time: "00:05:54"
    text: Todos os personagens são convocados
  - time: "00:10:37"
    text: No _Moulin Rouge_, Leandra e Said passam a missão aos personagens
  - time: "00:27:10"
    text: Começam as preparações com _Martin_ enviando seus homens para investigar o prédio da Sun Biotech
  - time: "00:31:15"
    text: _Jean-Jacques_ se infiltra como faxineiro na Sun Biotech como preparação
  - time: "00:34:38"
    text: _Camille_ procura obter informações ao usar seus contatos para achar alguém responsável pela segurança
  - time: "00:38:01"
    text: _Remy_ recorre a uma das _Elitès_, o Duque, e consegue uma série de _códigos, senhas e metadados_ para invasão de sistemas... Mas a que custo?
  - time: "00:48:44"
    text: _Camille_ obtêm informações extras por meio de uma engenharia social
  - time: "00:52:32"
    text: Começa a missão com os personagens chegando na Sun Biotech e conseguindo forjar uma reunião, passando uma primeira barreira de segurança
  - time: "01:08:49"
    text: No segundo nível, eles passam a barreira de segurança e conseguem obter uma forma de acessar o laboratório automatizado para buscar o ET-01
  - time: "01:40:24"
    text: No terceiro nível, eles tem mais problemas devido às excessivas notificações da A/R de experiências
  - time: "01:55:23"
    text: Remy filtra as _tags_ A/R até encontrar o _Project ET-01_
  - time: "02:02:59"
    text: Remy descobre um novo _Projeto Simuta_ para tornar a droga da V/R mais poderosa
  - time: "02:13:30"
    text: Jean-Jacques percebe algo errado quando o elevador falha... E o guarda descobre que "alguém" emperrou o elevador...
  - time: "02:17:30"
    text: Começa o _debrief_ e as acusações de traição, com Remy e Jean-Jacques revelando seus segredos
  - time: "02:36:31"
    text: Comentários Finais da Aventura
youtube_table: "fcIuPYf3ptg"
soundtrack:
  - music: Lewd Day
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Gorgonzola
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Anmbient Gourd
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Trumpet Backdrone
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: "Mary M's Space Music"
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Where the Honey Is
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Bassomatic
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: 303x909 (Remix)
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Gourd Travel
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Cost of Difference
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Floodwater
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Map of the Cosmos
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Schvisen
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Stringy
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: Sunflower
    artist: Canton Becker
    link: http://cantonbecker.com/music/
  - music: The Great River
    artist: Canton Becker
    link: http://cantonbecker.com/music/
---

Olá!

Na Mesa Predestinada 10, apresentamos mais aventuras de Remy e Jean-Jacques, os combatentes da _La Resisténce_, enfrentando o opressor governo de _Paris Nouveau_ e procurando derrubar o mesmo, com o auxílio de novos personagens: a _Blueblood_ Camile Desjardins e o _Officer_ Martin Le Marteur.

Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.


[fatemasters]: http://fatemasters.github.io
[rolandomaisquatro]: http://rolandomaisquatro.github.io
[camundongos-aventureiros]: https://pt.wikipedia.org/wiki/The_Country_Mouse_and_the_City_Mouse_Adventures
