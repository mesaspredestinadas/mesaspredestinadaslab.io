---
title: "Episódio 15 - Dungeon Tours LTD Playtest - O Ritual do Casal"
subheadline: Quatro antigos aventureiros ajudam a preparar um Bosque para um Ritual de Fertilidade e Amor para que os Amantes não morram no processo
date: 2017-03-03 08:15:00 -0300
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadast
 - Podcast
 - Dungeon Tours LTD Playtest
 - DTL
header: no
maisquatro_comments: true 
podcast_time: 166min
itunes:
  duration: 02:46:36
hosts:
  - Fábio Emilio Costa (Narrador)
  - Max Fischer (Fezair e Till)
  - Eduardo Olveira de Carvalho (Carvahal e Samir)
audios:
 - OGG: https://archive.org/download/MP15DungeonToursLTDPlaytest/MP15-DungeonToursLTD-Playtest.ogg
 - MP3: https://archive.org/download/MP15DungeonToursLTDPlaytest/MP15-DungeonToursLTD-Playtest.mp3
events:
  - time: "00:00:10"
    text: Introdução e Apresentação dos Jogadores e do Cenário
  - time: "00:06:23"
    text: A aventura começa quando os PCs recebem o pedido de Sarek, o Druida, para o ritual de Equinócio de Primavera
  - time: "00:18:04"
    text: Fezair e Smair vão conversar com os Amantes e com os Pais para descobrirem da determinação dos mesmos e sobre os riscos que podem ou não ser colocados
  - time: "00:29:56"
    text: Carvahal e Till vão conversar com os povos do Bosque Sagrado (desse ano)
  - time: "00:37:11"
    text: Sarek dá o prazo para os jogadores e começa a construção da "Dungeon" no Bosque Sagrado
  - time: "00:48:21"
    text: Os personagens começam a adicionar os perigos no Bosque
  - time: "01:38:22"
    text: Aparecem orcs de verdade e cobras para causarem uma bagunça no _Setback_, mas os jogadores lidam com isso para melhorar a "Dungeon"
  - time: "01:43:57"
    text: O último Esforço antes do Tour!
  - time: "01:46:29"
    text: "Os Amantes começam o Ritual, com uma péssima surpresa para os jogadores: os irmãos Cínicos dos dois!"
  - time: "02:15:05"
    text: O _Twist_ no Bosque, com o irmão da Lady percebendo que é tudo uma armação, e com a irmã do Lord provando ser orcofóbica e aumentando a bagunça toda!!!
  - time: "02:26:36"
    text: A última sala... E todos precisam evitar a _Canção dos Pixies do Amor Verdadeiro!_ 
  - time: "02:34:10"
    text: O Teste de Impressão Final, a conclusão da aventura, e o que os quatro personagens fizeram em seguida...
  - time: "02:38:24"
    text: Considerações Finais sobre a aventura e as regras
iaplayer: MP15DungeonToursLTDPlaytest
youtube_table: "nRCFriWUhRA"
comments: true
related_links:
  - text: Dungeon Tours LTD Playtest
    link: https://tatabletop.com/dungeon-tours-ltd/
  - text: As fichas usadas na aventura
    link: /assets/DungeonToursLTDFichas.pdf
--- 

![A Capa do Dungeon Tours LTD](/assets/img/dtl.jpg)

Olá!

A 15ª Mesa Predestinada é mais uma mesa de _playtest_, no caso de [_Dungeon Tours LTD_][dtl], um cenário de fantasia medieval e trapaças por Dave Joria, o mesmo autor de [_Masters of Umdaar_][mou] e do recente (e muito legal) [_Uranium Chef_][uranium], onde aventureiros experientes e cansados da vida nas masmorras voltam a usar suas habilidades para, a pedido de um Druida, criar uma "masmorra" em um Bosque Sagrado onde dois amantes, filhos de poderosos Lordes, terão que passar por desafios para provarem seu amor... E não morrer no processo, claro!

Pedimos desculpa por problemas na captação de áudio.

Abaixo o mapa da floresta...

![O Mapa do bosque](/assets/img/MP15-DungeonToursLTD-Playtest-Dungeon-Final.png)

E abaixo a lista de perigos que foram colocados em cada "sala"

| **Sala** | **Perigo**                                     | **Tipo** |
|:--------:|------------------------------------------------|:--------:|
| 1        | Espada na Pedra, obtem a Espada Falsa          | Falso    |
| 2        | Espinhos (sem ponta) e cobras                  | Real     |
| 3        | Lobo que vai escurecer a floresta              | Real     |
| 4        | Árvores Andantes (manipuladas pelas Criaturas) | Real     |
| 5        | Visão fantasmagórica Ilusória                  | Falso    |
| 6        | A Tentação da "Esfinge"                        | Falso    |
| 7        | Tribo de Orcs, obtem o Arco Fajuto             | Real     |
| 8        | Dragão "Vermelho Maligno"                      | Real     |
| 9        | Pixies da Canção do Amor Verdadeiro            | Real     |


Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.

[mou]: http://www.drivethrurpg.com/product/155458/Masters-of-Umdaar--A-World-of-Adventure-for-Fate-Core
[uranium]: http://www.drivethrurpg.com/product/205720/Uranium-Chef--A-World-of-Adventure-for-Fate-Core
[dtl]: https://tatabletop.com/dungeon-tours-ltd/
