---
title: "Episódio 12 - Loose Threads - Sessão 1 - Bicadas Mortais (Parte 1)"
subheadline: A Companhia começa suas aventuras, procurando limpar a maldição de uma meia-irmã de uma princesa
date: 2016-11-22 11:20:00 -0200
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadast
 - Podcast
 - Loose Threads
header: no
maisquatro_comments: true 
podcast_time: 120min
itunes:
  duration: 02:00:02
hosts:
  - Fábio Emilio Costa (Narrador)
  - Luiz Cavalheiro (Julian Brightsword)
  - Paulo "Meirense" França (Prático Schwein)
  - Bruna Nora (Condessa Sara Anastasia Terspichore)
  - Matheus Teru (Fang Sharpwise)
  - Nelly Coelho (Shimmer)
audios:
 - OGG: https://archive.org/download/MPEp12LooseThreads1/MP-Ep12-LooseThreads1.ogg
 - MP3: https://archive.org/download/MPEp12LooseThreads1/MP-Ep12-LooseThreads1.mp3
iaplayer: MPEp12LooseThreads1
comments: true
related_links:
  - text: Loose Threads
    link: http://www.drivethrurpg.com/product/196127/Loose-Threads--A-World-of-Adventure-for-Fate-Core
  - text: A Sessão 0 de Criação de Personagens de _Loose Threads_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/podcast/MP11-LooseThreads0/
  - text: Ficha do _Prático Schwein_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/PraticoSchwein/
  - text: Ficha de _Fang Sharpwise_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/FangSharpwise/
  - text: Ficha da _Shimmer_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/Shimmer/
  - text: Ficha da _Condessa Sara Anastasia Terspischore_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/Sara/
  - text: Ficha do _Julian Brightsword_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/JulianBrightsword/
events:
  - time: "00:00:10"
    text: Introdução, detalhes e ajustes antes de tudo começar
  - time: "00:15:05"
    text: A Companhia está em seu Acampamento quando Shimmer detecta um poderoso Desejo sendo pedido!
  - time: "00:26:50"
    text: Ao chegar na Cidade, a Companhia é recebida por uma Serviçal, que a conduz para uma casa em um Bosque... lotado de pássaros!
  - time: "00:29:39"
    text: Na casa com as janelas fechadas por tábuas e com uma lareira acesa, Elda, a meia-irmã, conta sua maldição e implora pelos serviços da Companhia
  - time: "00:37:30"
    text: A Condessa tem um _flashback_, visualizando tudo, e fica pasma, em estado de cacofonia
  - time: "00:44:39"
    text: Fang demanda recompensa... E Elda promete uma boa recompensa para a Companhia
  - time: "00:47:47"
    text: A Companhia vai conversar com a meia-irmã de Elda, a Princesa Marra, e conseguem alguma ajuda
  - time: "01:09:03"
    text: Sara se disfarça de camponesa para ouvir os rumores sobre o _Parlamento das Torres_, ou _Parlamento das Corujas_, que os camponeses mencionam, enquanto Prático prepara um apito de assustar pássaros
  - time: "01:19:09"
    text: Na Floresta, Prático usa o apito de assustar pássaros, o que chama a atenção do que não deveria
  - time: "01:30:57"
    text: Sara ouve piados vindos de uma direção oposta à qual a Companhia está indo, mas eles ignoram tais piados... E isso terá consequências no futuro
  - time: "01:33:07"
    text: Os caminhos começam a se bifurcar demais na Floresta, mas Shimmer consegue visualizar certos sinais místicos que indicam o caminho
  - time: "01:36:43"
    text: A Companhia chega ao acampamento do Conclave das Fadas Madrinhas, e Miriva se encontra com eles
  - time: "01:39:04"
    text: Miriva faz um amuleto para proteger Elda, mas demanda uma Promessa de _Ajudarem o Henry de Ferro_, que Sara assume
  - time: "01:45:36"
    text: A Chefe das Fadas Madrinhas desafia _A Companhia_ a provar sua posição... Com consequências sérias contra Prático, e Shimmer consegue resolver o desafio e permite que a Companhia siga adiante... para a próxima sessão
  - time: "01:55:50"
    text: Considerações finais 
youtube_table: "kQ2tvnLkesk"
soundtrack:
   - music: Angevin B
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Fiddles McGinty
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Heavy Heart
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Long Road Ahead
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Minstrel Guild
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Moorland
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Pippin  the Hunchback
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Thatched Villagers
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: The Medieval Banquet
     artist: "Silverman's Sound Studio"
     link: https://www.silvermansound.com/free-music/the-medieval-banquet
   - music: "Kitty's Ramble in New Orleans"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Silhouette of Longing Love
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/     
   - music: Stardust Serenade
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Johnny Jump Up
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Peggy Gordon
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Gypsy Rover
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Cats of Molly Malone
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: "Buttercup's Lament (Princess Bride)"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Catnipped Kitten
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: "Monahan's Mudder Milk"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Leprechaun
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Lusty Young Sith
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
---     

![[A _Companhia_ está encrencada! (__Da Esqueda para a Direita:__ Miriva, A Líder das Fadas Madrinhas, Prático, Sara, Fang e Shimmer) - Arte por Bruna Nora](/assets/LooseThreads1.jpg)](/assets/LooseThreads1.jpg)

Olá!

Na Mesa Predestinada 12, começa a Campanha de Loose Threads, com a _Companhia_ de desajustados habitantes de Contos de Fadas encontrando uma meia-irmã de uma Princesa que, atacada por pássaros continuamente, não consegue sair de uma pequena casinha em um bosque. Seu maior desejo é colocar os pés para fora de casa, mesmo agora tendo sido cegada pelos pássaros. A Companhia será capaz de realizar esse Desejo? E a qual custo?

Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.


[fatemasters]: http://fatemasters.github.io
[rolandomaisquatro]: http://rolandomaisquatro.github.io
[camundongos-aventureiros]: https://pt.wikipedia.org/wiki/The_Country_Mouse_and_the_City_Mouse_Adventures
