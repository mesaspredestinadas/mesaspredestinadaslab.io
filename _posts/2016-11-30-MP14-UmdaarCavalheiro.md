---
title: "Episódio 14 - O Congresso dos Mestres de Umdaar - A Espada do Destino"
subheadline: O Cicerone do Fate Horror apresenta uma nova visão de _Masters of Umdaar_, quando os Mestres de Umdaar se unem em um Congresso
date: 2016-11-30 11:30:00 -0200
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadast
 - Podcast
 - Masters-of-Umdaar
header: no
maisquatro_comments: true 
podcast_time: 158min
itunes:
  duration: 02:28:33
hosts:
  - Luís Cavalheiro (Narrador)
audios:
 - OGG: https://archive.org/download/MPEp14UmdaarCavalheiro/MP-Ep14-UmdaarCavalheiro.ogg
 - MP3: https://archive.org/download/MPEp14UmdaarCavalheiro/MP-Ep14-UmdaarCavalheiro.mp3
iaplayer: MPEp14UmdaarCavalheiro
youtube_table: "VN977jmnFyU"
comments: true
related_links:
  - text: Masters of Umdaar
    link: http://www.drivethrurpg.com/product/155458/Masters-of-Umdaar--A-World-of-Adventure-for-Fate-Core
  - text: As fichas usadas na aventura
    link: https://archive.org/download/MPEp14UmdaarCavalheiro/FichasCongressoUmdaar.zip
--- 

Olá!

A Mesa Predestinada 14 é uma mesa diferente especial: dá lugar ao Mister Mickey Fábio como narrador o Cicerone do Fate Horror Luís Cavalheiro. E ele ataca em Umdaar, publicando a mesa que ele narrou na no evento _RPG na BPE_, no Rio de Janeiro.

Os Mestres de Umdaar resolveram realizar um congresso em Cerrato Basin para elaborar um plano sinistro para conquistar o planeta. Conseguirão os Arqueonautas impedi-los?

Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.


