---
title: "Episódio 13 - Loose Threads - Sessão 2 - Bicadas Mortais (Parte 2)"
subheadline: A Companhia conseguiu o Amuleto, mas a Floresta cobra o preço pela bagunça que eles aprontaram!
date: 2016-11-24 10:40:00 -0200
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadast
 - Podcast
 - Loose Threads
header: no
maisquatro_comments: true 
podcast_time: 126min
itunes:
  duration: 02:06:30
hosts:
  - Fábio Emilio Costa (Narrador e Julian Brightsword)
  - Paulo "Meirense" França (Prático Schwein)
  - Bruna Nora (Condessa Sara Anastasia Terspichore)
  - Matheus Teru (Fang Sharpwise)
  - Nelly Coelho (Shimmer)
audios:
 - OGG: https://archive.org/download/MPEp13LooseThreads2/MP-Ep13-LooseThreads2.ogg
 - MP3: https://archive.org/download/MPEp13LooseThreads2/MP-Ep13-LooseThreads2.mp3
iaplayer: MPEp13LooseThreads2
comments: true
related_links:
  - text: Loose Threads
    link: http://www.drivethrurpg.com/product/196127/Loose-Threads--A-World-of-Adventure-for-Fate-Core
  - text: A Sessão 1 de Criação de Personagens de _Loose Threads_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/podcast/MP12-LooseThreads2/
  - text: Ficha do _Prático Schwein_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/PraticoSchwein/
  - text: Ficha de _Fang Sharpwise_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/FangSharpwise/
  - text: Ficha da _Shimmer_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/Shimmer/
  - text: Ficha da _Condessa Sara Anastasia Terspischore_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/Sara/
  - text: Ficha do _Julian Brightsword_
    link: http://mesaspredestinadas.gitlab.io/mesas-predestinadas/personagens/JulianBrightsword/
events:
  - time: "00:00:10"
    text: Introdução, e recaptulação
  - time: "00:02:10"
    text: A Companhia se perde um pouco na Floresta, pois o Sapatinho de Cristal está apontando para dentro da Floresta
  - time: "00:08:04"
    text: A Companhia se envolve em nova confusão com uma matilha de Ursos, e isso terá consequências sérias!
  - time: "00:39:30"
    text: Sara realiza um Pedido para Shimmer, e ela recorre à sua Grande Magia para teletransportar todos de volta para cidade... Com uma má surpresa os esperando lá
  - time: "00:43:33"
    text: Os pássaros da cidade começam a atacar a Companhia devido ao derramamento de sangue na Floresta!
  - time: "00:50:44"
    text: Sara consegue, mesmo sob bicadas, entregar o amuleto para Elda e pegar a recompensa, retornando com uma tocha!
  - time: "00:55:37"
    text: Shimmer sobe nas tamancas (___que ela não tem___) e espinafra todo mundo, sejam pássaros seja a Companhia pela bagunça toda!
  - time: "00:58:44"
    text: Fang ___ARRANCA A CABEÇA DE UM PÁSSARO AO MELHOR ESTILO OZZY OZBORNE___... E os pássaros respondem à altura, arrancando pelo, presas e garras de Fang ___A BICADAS!___ (YIKES!!!!!)
  - time: "01:01:38"
    text: Prático corre enlouquecido atrás dos pássaros que estão levando Fang para a Floresta
  - time: "01:04:35"
    text: Shimmer detecta um Desejo do Coração na Floresta... Que pode ajudar a Companhia... E Sara a acompanha
  - time: "01:09:15"
    text: Prático encontra Fang em um estado extremamente lastimável, e cheio de Medo constrói um abrigo para Fang, enquanto é observado pelo Parlamento das Corujas, entrando em um _Flashback_
  - time: "01:14:51"
    text: Shimmer encontra um Castelo na Floresta e descobre a Corte dos Gatos Amaldiçoados, sua líder Alba, a história dos mesmos e como quebrar a maldição dos mesmos... com Julian Brightsword!!!!
  - time: "01:21:07"
    text: Prático conta sua história e sacrifica seu orgulho de certa forma... E ao contar sua história, Prático ganha a simpatia do Primeiro Ministro do Parlamento das Corujas e descobre o que aconteceu com a Mãe e a Irmã de Elda
  - time: "01:29:20"
    text: Shimmer e Sara procuram Julian e levam ele até Alba, realizando o Desejo do Coração de Julian e começando a quebrar a maldição de Alba, e com isso a Líder das Fadas Madrinhas rompe a maldição de Elda de vez.
  - time: "01:38:38"
    text: A Conclusão da Aventura, onde Marra e Elda se abraçam chorando de saudade e felicidade.... E as fitas de ferro brilham em direção ao Leste, a direção para a qual a Companhia segue, sem as maldições sob Fang e Prático
  - time: "01:43:54"
    text: A Evolução dos Personagens
  - time: "01:53:33"
    text: Comentários Finais
youtube_table: "Hj2DCevAW2c"
soundtrack:
   - music: "What Child is This? (Greensleeves)"
     artist: Admiral Bob
     link: http://ccmixter.org/files/admiralbob77/34685
   - music: Angevin B
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Fiddles McGinty
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Heavy Heart
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Long Road Ahead
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Minstrel Guild
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Moorland
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Pippin  the Hunchback
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Thatched Villagers
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: The Medieval Banquet
     artist: "Silverman's Sound Studio"
     link: https://www.silvermansound.com/free-music/the-medieval-banquet
   - music: "Kitty's Ramble in New Orleans"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Silhouette of Longing Love
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/     
   - music: Stardust Serenade
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Johnny Jump Up
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Peggy Gordon
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Gypsy Rover
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Cats of Molly Malone
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: "Buttercup's Lament (Princess Bride)"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Catnipped Kitten
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: "Monahan's Mudder Milk"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Leprechaun
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Lusty Young Sith
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
---     

Olá!

Na Mesa Predestinada 13 segue  a Campanha de Loose Threads. A _Companhia_ obteve com Miriva, a Fada Madrinha, o Amuleto de proteção contra os passáros que atacam Elda. Mas depois de todas as bagunças que a mesma provocou na sessão anterior, eles não poderiam sair impunes, e a Floresta decide cobrar o preço pela bagunça. Quais deles irão sofrer? Algum deles poderá alcançar em definitivo o Desejo do Coração? Ou estarão eles fadados a sofrerem no meio da Floresta? As Fábulas são perigosas, e o poder da Magia é algo temeroso. Nada é sem um preço nas Fábulas e a _Companhia_ tem que pagar o preço pelo que fizeram.

Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.


