---
title: "Episódio 11 - Loose Threads - Sessão 0 - Criação de Personagem"
subheadline: Começamos a campanha de Loose Threads, criando a Companhia de Desasjustados para os quais o "Felizes para Sempre" não aconteceu
date: 2016-11-17 15:50:00 -0200
layout: post
categories:
  - Mesas-Predestinadas
  - Podcast
tags:
 - Fate
 - Fate-Core
 - Mesas-Predestinadas
 - Podcast
 - Loose Threads
header: no
maisquatro_comments: true 
podcast_time: 189min
itunes:
  duration: 03:09:01
hosts:
  - Fábio Emilio Costa (Narrador)
  - Luiz Cavalheiro (Julian Brightsword)
  - Paulo "Meirense" França (Prático Schwein)
  - Bruna Nora (Condessa Sara Anastasia Terspichore)
  - Matheus Teru (Fang Sharpwise)
  - Nelly Coelho (Shimmer)
audios:
 - OGG: https://archive.org/download/MPEp11LooseThreads0/MP-Ep11-LooseThreads0.ogg
 - MP3: https://archive.org/download/MPEp11LooseThreads0/MP-Ep11-LooseThreads0.mp3
iaplayer: MPEp11LooseThreads0
comments: true
related_links:
  - text: Loose Threads
    link: http://www.drivethrurpg.com/product/196127/Loose-Threads--A-World-of-Adventure-for-Fate-Core
  - text: Ficha do _Prático Schwein_
    link: /personagens/PraticoSchwein/
  - text: Ficha de _Fang Sharpwise_
    link: /personagens/FangSharpwise/
  - text: Ficha da _Shimmer_
    link: /personagens/Shimmer/
  - text: Ficha da _Condessa Sara Anastasia Terspischore_
    link: /personagens/Sara/
  - text: Ficha do _Julian Brightsword_
    link: /personagens/JulianBrightsword/
  - text: Mr. Fox (o Conto citado no Trio de Fases - em inglês)
    link: http://www.surlalunefairytales.com/bluebeard/stories/mrfox.html
  - text: O Senhor Fox (conto do Trio de Fases em porutugês)
    link: http://contosvelhos.blogspot.com.br/2016/02/o-senhor-fox-ingles.html
events:
  - time: "00:00:11"
    text: Introdução, apresentação dos jogadores e do cenário
  - time: "00:05:24"
    text: Comentários prévios sobre a criação dos personagens
  - time: "00:06:26"
    text: Começando a criar o personagem de Meirense, _Prático_, definindo Aspectos, Perícias importantes e Façanhas
  - time: "00:24:39"
    text: Começando a criar o personagem de Matheus, _Fang_
  - time: "00:40:21"
    text: Começando a criar o personagem da Bruna, _Condessa Sara Anastasia Terspichore_
  - time: "01:03:24"
    text: Começando a criar o personagem do Luís, _Julian Brightsword_
  - time: "01:20:21"
    text: Começando a criar o personagem da Nelly, _Shimmer_
  - time: "01:53:27"
    text: Começando o Trio de Fases (ou, derrotando o _Senhor Raposo_)
  - time: "02:13:25"
    text: Começando as Relações dentro do Trio de Fases (com comentários políticos)
  - time: "02:29:10"
    text: Terminando o Trio de Fases, descrevendo novas Relações
  - time: "02:46:10"
    text: Arrematando os personagens, dentro do que os jogadores acharem válidos
  - time: "03:02:02"
    text: Considerações Finais
youtube_table: "3x2Zw2ckWc8"
soundtrack:
   - music: Angevin B
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Fiddles McGinty
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Heavy Heart
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Long Road Ahead
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Minstrel Guild
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Moorland
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Pippin  the Hunchback
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: Thatched Villagers
     artist: Kevin Macleod
     link: https://incompetech.com/wordpress/author/kevin/
   - music: The Medieval Banquet
     artist: "Silverman's Sound Studio"
     link: https://www.silvermansound.com/free-music/the-medieval-banquet
   - music: "Kitty's Ramble in New Orleans"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Silhouette of Longing Love
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/     
   - music: Stardust Serenade
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Johnny Jump Up
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Peggy Gordon
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Gypsy Rover
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Cats of Molly Malone
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: "Buttercup's Lament (Princess Bride)"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: Catnipped Kitten
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: "Monahan's Mudder Milk"
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Leprechaun
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
   - music: The Lusty Young Sith
     artist: Marc Gunn
     link: http://www.marcgunn.com/folk_music/
---     
Olá!

Na Mesa Predestinada 11, começamos uma nova campanha, dessa vez de _Loose Threads_, cenário de Contos de Fadas para Fate baseado nos _World of Adventure_. E para começar, criamos os personagens da Companhia, e falamos como eles deram conta do Senhor Raposo em suas primeiras avnenturas! Vejamos como esses enjeitados, para quem o "Felizes Para Sempre" não ocorreu, conseguirão ter seus desejos do coração realizados.

Nosso contato para esse podcast é o _email_ <mesaspredestinadas@gmail.com>, entre outros contatos a serem estipulados.


[fatemasters]: http://fatemasters.github.io
[rolandomaisquatro]: http://rolandomaisquatro.github.io
[camundongos-aventureiros]: https://pt.wikipedia.org/wiki/The_Country_Mouse_and_the_City_Mouse_Adventures
